unit u_Ecuacion_General_de_Segundo_Grado;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, ExtCtrls,
  Spin, ComCtrls, Math;

type

  { TForm1 }

  TForm1 = class(TForm)
    b_Calcular: TButton;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    ImageList1: TImageList;
    tv_resultados: TTreeView;
    valorA: TFloatSpinEdit;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Panel1: TPanel;
    Panel2: TPanel;
    valorC: TFloatSpinEdit;
    valorB: TFloatSpinEdit;
    procedure calcularSolucion(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private

  public

  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.FormCreate(Sender: TObject);
begin

end;

procedure TForm1.calcularSolucion(Sender: TObject);
var
  a, b, c,
  x1, x2,
  disc   : Real;

  nodo   : TTreeNode;
begin
  a := valorA.Value;
  b := valorB.Value;
  c := valorC.Value;
  disc := power(b,2)-4*a*c;
  if disc >= 0 then
  begin
    {calcular las soluciones}
    x1 := (-1*b+sqrt(disc))/(2*a);
    x2 := (-1*b-sqrt(disc))/(2*a);

    nodo := TTreeNode.Create(nil);
    nodo := tv_resultados.Items.Add(nodo,'x1='+FloatToStr(x1)+'; x2='+FloatToStr(x2));

    nodo.ImageIndex := 0;
    nodo.SelectedIndex := 0;
  end
  else
  begin
    {mostrar un error}
    nodo := TTreeNode.Create(nil);
    nodo := tv_resultados.Items.Add(nodo,'Error: La ecuación no tiene solución porque disc='+FloatToStr(disc));

    nodo.ImageIndex := 1;
    nodo.SelectedIndex := 1;
  end;
end;

end.

